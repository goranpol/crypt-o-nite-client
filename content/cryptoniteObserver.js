function CryptoniteObserver()
{
	this.init();
}

CryptoniteObserver.prototype =
{
	_observerService : null,
	_anchor : null,
	
	init: function()
	{
		this._observerService = Components.classes["@mozilla.org/observer-service;1"].getService(Components.interfaces.nsIObserverService);
		
		// add all observers
		this._observerService.addObserver(this, "crypt-o-nite-encrypt", false);
		this._observerService.addObserver(this, "crypt-o-nite-decrypt", false);
		
		this._anchor = document.getElementById("cryptoniteLogoutButton");
		
		return this;
	},

	unregisterObserver: function()
	{
		// remove all observers
		this._observerService.removeObserver(this, "crypt-o-nite-encrypt");
		this._observerService.removeObserver(this, "crypt-o-nite-decrypt");
		
		this._observerService = null;
	},
	
	notifyObservers: function(aTopic, aMessage)
	{
		this._observerService.notifyObservers(null, aTopic, aMessage);
	},

	observe: function(request, aTopic, aData)
	{
		// crypt-o-nite-encrypt
		// crypt-o-nite-decrypt
		try
		{
			var doPopup = true;
			
			if(aTopic == "crypt-o-nite-encrypt")
			{
				this.addIntoLog("encrypting " + aData);
			}
			else if(aTopic == "crypt-o-nite-decrypt")
			{
				this.addIntoLog("decrypting " + aData);
			}
			
			if(doPopup == true)
				document.getElementById("cryptoniteMessagePanel").openPopup(this._anchor, "", 0, 0, false, false);
		}
		catch(err)
		{
		}
	},
	
	addIntoLog: function(data)
	{
		/* var container = document.getElementById("cryptoniteActionMessages");
		var newLabel = document.createElement("label");
		newLabel.setAttribute("value", data);
		newLabel.setAttribute("class", "labelsSmallBlack");
		container.insertBefore(newLabel, container.firstChild); */
		document.getElementById("cryptoniteMessagePanelLabel").setAttribute("value", data);
	}
}