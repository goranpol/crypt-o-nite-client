var Cryptonite =
{
	_cryptonite: null,
	_cryptoniteObserver: null,
	
	login: function(aEvent)
	{
		try
		{
			Components.utils.import("resource://gre/modules/ctypes.jsm");
			
			const ioService = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService);
			var uri = ioService.newURI('resource://cryptoniteLib', null, null);
			if (uri instanceof Components.interfaces.nsIFileURL)
			{
				this._cryptonite = ctypes.open(uri.file.path);
			}
			
			var username = document.getElementById("cryptoniteUsername").value;
			var password = document.getElementById("cryptonitePassword").value;
			
			var usernameChar = ctypes.char.array()(username);
			var passwordChar = ctypes.char.array()(password);
			
			var loginCall = this._cryptonite.declare("CLogin",					// function name
													ctypes.default_abi,			// call ABI
													ctypes.char.ptr,			// return type
													ctypes.char.ptr,			// argument type
													ctypes.char.ptr				// argument type
													);
			
			var userDetails = loginCall(usernameChar, passwordChar);			
			
			document.getElementById("cryptoniteLoginPanel").hidePopup();
			
			try
			{
				var userDetailJson = JSON.parse(userDetails.readString());
				CryptoniteLoginData.init(userDetailJson.ud.u1, userDetailJson.ud.u2, userDetailJson.ud.u3, userDetailJson.ud.u4, userDetailJson.ud.u5);
				
				document.getElementById("cryptoniteLoginButton").hidden = true;
				document.getElementById("cryptoniteLogoutButton").hidden = false;
				document.getElementById("cryptoniteLogoutButton").label = userDetailJson.username;
				// document.getElementById("cryptoniteEncryptButton").hidden = false;
				
				this._cryptoniteObserver = new CryptoniteObserver();
			}
			catch(err)
			{
				try
				{
					var errorJson = JSON.parse(userDetails.readString());
					if(errorJson.errm == "error with validation")
						errorJson.errm = "username or password incorrect";
					
					alert(errorJson.errm);
				}
				catch(err)
				{
					alert("ERROR: " + err);
				}
			}
		}
		catch(err)
		{
			alert("ERROR: " + err);
		}
	},
	
	logout: function(aEvent)
	{
		try
		{
			if(this._cryptoniteObserver != null)
			{
				this._cryptoniteObserver.unregisterObserver();
				this._cryptoniteObserver = null;
			}
			CryptoniteLoginData.uninit();
			
			var logoutCall = this._cryptonite.declare("CLogout",			// function name
										ctypes.default_abi,					// call ABI
										ctypes.void_t						// return type
										);
			
			logoutCall();
			gBrowser.reload(true);
			
			document.getElementById("cryptoniteLoginButton").hidden = false;
			document.getElementById("cryptoniteLogoutButton").hidden = true;
			// document.getElementById("cryptoniteEncryptButton").hidden = true;
		}
		catch(err)
		{
			alert("ERROR: " + err);
		}
	}
}
