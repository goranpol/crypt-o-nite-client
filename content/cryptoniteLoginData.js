var CryptoniteLoginData =
{
	_u1: null,
	_u2: null,
	_u3: null,
	_u4: null,
	_u5: null,
	_logedInGMail: null,
	_logedInGDrive: null,
	_logedInHotmail: null,
	_logedInOneDrive: null,
	
	init: function(u1, u2, u3, u4, u5)
	{
		this._u1 = u1;
		this._u2 = u2;
		this._u3 = u3;
		this._u4 = u4;
		this._u5 = u5;
		
		this._logedInGMail = false;
		this._logedInGDrive = false;
		this._logedInHotmail = false;
		this._logedInOneDrive = false;
		
		gBrowser.addEventListener("pageshow", this.onLoad, false);
		CryptoniteProgressListener.init();
		checkLogedInUser(gBrowser.currentURI);
	},
	
	uninit: function()
	{
		this._u1 = null;
		this._u2 = null;
		this._u3 = null;
		this._u4 = null;
		this._u5 = null;
		
		this._logedInGMail = null;
		this._logedInGDrive = null;
		this._logedInHotmail = null;
		this._logedInOneDrive = null;
		
		gBrowser.removeEventListener("pageshow", this.onLoad, false);
		CryptoniteProgressListener.uninit();
	},
	
	onLoad: function(event)
	{
		// put others in as they come
		var ret = false;
		if(gBrowser.currentURI.spec.search("mail.google.com") != -1 || gBrowser.currentURI.spec.search("drive.google.com") != -1 || gBrowser.currentURI.spec.search("document.google.com") != -1 || gBrowser.currentURI.spec.search("mail.live.com") != -1)
		{
			try
			{
				ret = checkLogedInUser(gBrowser.currentURI);
			}
			catch(err)
			{
			}
		}
	},
	
	toString: function()
	{
		return this._u1 + ' ' + this._u2 + ' ' + this._u3 + ' ' + this._u4 + ' ' + this._u5;
	}
}

var CryptoniteProgressListener =
{	
	init: function()
	{
		gBrowser.addProgressListener(this);
	},

	uninit: function()
	{
		gBrowser.removeProgressListener(this);
	},

	// nsIWebProgressListener
	QueryInterface: XPCOMUtils.generateQI(["nsIWebProgressListener", "nsISupportsWeakReference"]),

	onLocationChange: function(aProgress, aRequest, aURI)
	{
		var ret = false;
		if(aURI.spec.search("mail.google.com") != -1 || aURI.spec.search("drive.google.com") != -1 || aURI.spec.search("document.google.com") != -1 || aURI.spec.search("mail.live.com") != -1 || aURI.spec.search("onedrive.live.com") != -1 || aURI.spec.search("outlook.live.com") != -1)
		{
			ret = checkLogedInUser(aURI);
		}
		
		if(ret == false)
			return;
	},

	// implement at least one of thoose functions
	onStateChange: function() {},
	onProgressChange: function() {},
	onStatusChange: function() {},
	onSecurityChange: function() {}
}

function checkUsername(username)
{
	if (CryptoniteLoginData._u1 == username)
		return true;
	else if (CryptoniteLoginData._u2 == username)
		return true;
	else if (CryptoniteLoginData._u3 == username)
		return true;
	else if (CryptoniteLoginData._u4 == username)
		return true;
	else if (CryptoniteLoginData._u5 == username)
		return true;
	else
		return false;
}

function checkLogedInUser(aURI)
{
	try
	{
		if(aURI.spec.search("mail.google.com") != -1)
		{
			var win = window.top.getBrowser().selectedBrowser.contentWindow;
			
			try
			{
				win.wrappedJSObject.GLOBALS[10];
			}
			catch(err)
			{
				return false;
			}
				
			if(checkUsername(win.wrappedJSObject.GLOBALS[10]))
			{
				if(CryptoniteLoginData._logedInGMail != true)
				{
					CryptoniteLoginData._logedInGMail = true;
					CryptoniteLoginData._logedInGDrive = true;
					Components.classes["@mozilla.org/observer-service;1"].getService(Components.interfaces.nsIObserverService).notifyObservers(null, "crypt-o-nite-change-flags", "{\"name\": \"gmail\", \"value\": true}");
					Components.classes["@mozilla.org/observer-service;1"].getService(Components.interfaces.nsIObserverService).notifyObservers(null, "crypt-o-nite-change-flags", "{\"name\": \"gdrive\", \"value\": true}");
					gBrowser.reload(true);
				}
				
				return true;
			}
			
			if(CryptoniteLoginData._logedInGMail != false)
			{		
				CryptoniteLoginData._logedInGMail = false;
				Components.classes["@mozilla.org/observer-service;1"].getService(Components.interfaces.nsIObserverService).notifyObservers(null, "crypt-o-nite-change-flags", "{\"name\": \"gmail\", \"value\": false}");
			}
			
			return false;
		}
		else if(aURI.spec.search("drive.google.com") != -1)
		{
			var win = window.top.getBrowser().selectedBrowser.contentWindow;
			if(checkUsername(win.wrappedJSObject.__initData[0][27][8]))
			{
				if(CryptoniteLoginData._logedInGDrive != true)
				{			
					CryptoniteLoginData._logedInGDrive = true;
					Components.classes["@mozilla.org/observer-service;1"].getService(Components.interfaces.nsIObserverService).notifyObservers(null, "crypt-o-nite-change-flags", "{\"name\": \"gdrive\", \"value\": true}");
				}
				
				return true;
			}
			
			if(CryptoniteLoginData._logedInGDrive != false)
			{
				CryptoniteLoginData._logedInGDrive = false;
				Components.classes["@mozilla.org/observer-service;1"].getService(Components.interfaces.nsIObserverService).notifyObservers(null, "crypt-o-nite-change-flags", "{\"name\": \"gdrive\", \"value\": false}");
			}
			
			return false;
		}
		else if(aURI.spec.search("docs.google.com") != -1)
		{
			var win = window.top.getBrowser().selectedBrowser.contentWindow;
			if(checkUsername(win.wrappedJSObject._docs_flag_initialData.email))
			{
				if(CryptoniteLoginData._logedInGDrive != true)
				{			
					CryptoniteLoginData._logedInGDrive = true;
					Components.classes["@mozilla.org/observer-service;1"].getService(Components.interfaces.nsIObserverService).notifyObservers(null, "crypt-o-nite-change-flags", "{\"name\": \"gdrive\", \"value\": true}");
				}
				
				return true;
			}
			
			if(CryptoniteLoginData._logedInGDrive != false)
			{
				CryptoniteLoginData._logedInGDrive = false;
				Components.classes["@mozilla.org/observer-service;1"].getService(Components.interfaces.nsIObserverService).notifyObservers(null, "crypt-o-nite-change-flags", "{\"name\": \"gdrive\", \"value\": false}");
			}
			
			return false;
		}
		else if(aURI.spec.search("mail.live.com") != -1)
		{
			var win = window.top.getBrowser().selectedBrowser.contentWindow;
			if(checkUsername(win.wrappedJSObject.$Config.email))
			{
				if(CryptoniteLoginData._logedInHotmail != true)
				{
					CryptoniteLoginData._logedInHotmail = true;
					Components.classes["@mozilla.org/observer-service;1"].getService(Components.interfaces.nsIObserverService).notifyObservers(null, "crypt-o-nite-change-flags", "{\"name\": \"hotmail\", \"value\": true}");
					gBrowser.reload(true);
				}
				
				return true;
			}
			
			if(CryptoniteLoginData._logedInHotmail != false)
			{
				CryptoniteLoginData._logedInHotmail = false;
				Components.classes["@mozilla.org/observer-service;1"].getService(Components.interfaces.nsIObserverService).notifyObservers(null, "crypt-o-nite-change-flags", "{\"name\": \"hotmail\", \"value\": false}");
			}
		}
		else if(aURI.spec.search("onedrive.live.com") != -1)
		{
			var win = window.top.getBrowser().selectedBrowser.contentWindow;
			if(checkUsername(win.wrappedJSObject.$Config.email))
			{
				if(CryptoniteLoginData._logedInOneDrive != true)
				{
					CryptoniteLoginData._logedInOneDrive = true;
					Components.classes["@mozilla.org/observer-service;1"].getService(Components.interfaces.nsIObserverService).notifyObservers(null, "crypt-o-nite-change-flags", "{\"name\": \"onedrive\", \"value\": true}");
					// gBrowser.reload(true);
				}
				
				return true;
			}
			
			if(CryptoniteLoginData._logedInOneDrive != false)
			{
				CryptoniteLoginData._logedInOneDrive = false;
				Components.classes["@mozilla.org/observer-service;1"].getService(Components.interfaces.nsIObserverService).notifyObservers(null, "crypt-o-nite-change-flags", "{\"name\": \"onedrive\", \"value\": false}");
			}
		}
		else if(aURI.spec.search("outlook.live.com") != -1)
		{
			var win = window.top.getBrowser().selectedBrowser.contentWindow;
			if(checkUsername(JSON.parse(win.wrappedJSObject.owaSDState.data).owaUserConfig.SessionSettings.LogonEmailAddress))
			{
				if(CryptoniteLoginData._logedInHotmail != true)
				{
					CryptoniteLoginData._logedInHotmail = true;
					Components.classes["@mozilla.org/observer-service;1"].getService(Components.interfaces.nsIObserverService).notifyObservers(null, "crypt-o-nite-change-flags", "{\"name\": \"hotmail\", \"value\": true}");
					gBrowser.reload(true);
				}
				
				return true;
			}
			
			if(CryptoniteLoginData._logedInHotmail != false)
			{
				CryptoniteLoginData._logedInHotmail = false;
				Components.classes["@mozilla.org/observer-service;1"].getService(Components.interfaces.nsIObserverService).notifyObservers(null, "crypt-o-nite-change-flags", "{\"name\": \"hotmail\", \"value\": false}");
			}
		}
	}
	catch(err)
	{
		// you can put it out later
		// alert("ERROR");
	}
	// others
}
	